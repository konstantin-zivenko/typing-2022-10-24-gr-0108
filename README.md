# CyBionic_PythonAdvanced_6_typing

Матеріали до лекції "Типізований Python" курсу PythonAdvanced від CyberBionic. Розглядаємо можливості та використання модуля typing на прикладі.

Побудовано на розборі та деякій переробці матеріалів [статьи](https://realpython.com/python-type-checking/)


[video](https://youtu.be/2MhHpQMbLao)
def prepare_str(text: str, proper_noun: bool = False) -> str:
    result = text.strip().lower()
    if proper_noun:
        result = result.capitalize()
    return result


print(prepare_str("PLANE"))
print(prepare_str("bob", proper_noun=True))
print(prepare_str("MARY", proper_noun=True))
print(prepare_str("KEVIN", proper_noun=True))
